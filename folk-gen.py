#!/usr/bin/python3

import sys
import tensorflow as tf
import numpy as np
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences


vocab_size = 10000
datafile = "../data/zpevnik.txt"


def prepare_data():
    corpus = []
    with open(datafile) as f:
        corpus = f.read().lower().split("\n")
    tok = Tokenizer(num_words=vocab_size, oov_token="<OOV>")
    tok.fit_on_texts(corpus)
    return tok


def predict(model, text):
    input_len = model.layers[0].input.shape[1]
    for _ in range(10):
        xs = pad_sequences(tok.texts_to_sequences([text]), maxlen=input_len, padding="pre", truncating="pre")
        ys = model.predict(xs)[0]
        while True:
            predicted = np.argmax(ys)
            if predicted > 1:
                break
            ys[predicted] = 0
        text = text + " " + tok.index_word[predicted]
    print(text)


if len(sys.argv) < 2:
    print(f"Usage: {sys.argv[0]} sentence")
    sys.exit(1)

tok = prepare_data()
model = tf.keras.models.load_model("folk.h5")
predict(model, " ".join(sys.argv[1:]))

