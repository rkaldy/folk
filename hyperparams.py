from collections import OrderedDict
import sys

class Hyperparams:
    def __init__(self, defs):
        self.defs = OrderedDict()
        for df in defs:
            self.defs[df[0]] = df[1]
        self.count = 1
        for vals in self.defs.values():
            self.count *= len(vals)
    
    def __iter__(self):
        self.idx = 0
        return self

    def __next__(self):
        if self.idx == self.count:
            raise StopIteration
        idx = self.idx
        params = {}
        for key, vals in reversed(self.defs.items()):
            params[key] = vals[idx % len(vals)]
            idx //= len(vals)
        self.idx += 1
        return params

    def values(self, key):
        return self.defs[key]

    def endOfCycle(self, key):
        n = 1
        for k, v in reversed(self.defs.items()):
            n *= len(v)
            if k == key:
                break
        return self.idx % n == 0

    def pack(self, params, suppress_keys = []):
        return "_".join([str(v) for k, v in params.items() if not k in suppress_keys])

    def dump(self, params, suppress_keys = []):
        return " ".join([k + "=" + str(params[k]) for k in self.defs.keys() if not k in suppress_keys])
