#!/usr/bin/python3 

import sys, math, bisect, os, glob
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from hyperparams import Hyperparams
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences


vocab_size = 20000
num_epochs = 100
datafile = "zpevnik.txt"


def prepare_data():
    corpus = []
    with open(datafile) as f:
        corpus = f.read().lower().split("\n")
    
    tok = Tokenizer(num_words=vocab_size, oov_token="<OOV>")
    tok.fit_on_texts(corpus)
    seqs = [seq for seq in tok.texts_to_sequences(corpus) if len(seq) >= 2]
    maxlen = max([len(x) for x in seqs])
    return seqs, maxlen - 1


class DataGenerator(tf.keras.utils.Sequence):
    def __init__(self, seqs, maxlen):
        self.seqs = seqs
        self.maxlen = maxlen
        self.index = [0]
        for seq in seqs:
            self.index.append(self.index[-1] + len(seq) - 1)

    def __len__(self):
        return math.ceil(self.index[-1] / self.batch_size)

    def __getitem__(self, index):
        si = bisect.bisect_right(self.index, index) - 1
        wi = index - self.index[si] + 1
        xs = []
        ys = []
        for bi in range(self.batch_size):
            xs.append(self.seqs[si][:wi])
            ys.append(self.seqs[si][wi])
            wi += 1
            if wi >= len(self.seqs[si]):
                wi = 1
                si += 1
            if si >= len(self.seqs):
                break
        xs = pad_sequences(xs, maxlen, padding="pre")
        ys = tf.keras.utils.to_categorical(ys, num_classes=vocab_size)
        return xs, ys


def create_model(embed_dim, lstm_size, dropout_rate, input_len):
    model = tf.keras.Sequential([
        tf.keras.layers.Embedding(vocab_size, embed_dim, input_length=input_len),
        tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(lstm_size, dropout=dropout_rate, recurrent_dropout=0)),
        tf.keras.layers.Dropout(dropout_rate),
        tf.keras.layers.Dense(vocab_size, activation="softmax")
    ])
    model.compile(loss="categorical_crossentropy", optimizer="Adam", metrics=["accuracy"])
    return model


def predict(model, text, input_len):
    for _ in range(10):
        seq = pad_sequences(tok.texts_to_sequences([text]), maxlen=input_len, padding="pre", truncating="pre")
        predicted = np.argmax(model.predict(seq))
        if predicted != 0:
            text = text + " " + tok.index_word[predicted]
    print(text)


def plot_graphs(history):
    plt.plot(history.history["accuracy"])
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")


def save_graph(hyper, params, legend):
    plt.title(hyper.dump(params, [legend]))
    plt.legend([legend + "=" + str(v) for v in hyper.values(legend)])
    filename = "acc_" + hyper.pack(params, [legend]) + ".png"
    print(f"Saving graphs to {filename}")
    plt.savefig(filename)
    plt.figure()


for f in glob.glob("acc_*.png"):
    os.remove(f)

(seqs, maxlen) = prepare_data()
gen = DataGenerator(seqs, maxlen)

hyper = Hyperparams([
    ("embed_dim", (16, 32, 64, 128)),
    ("lstm_size", (32, 64, 128)),
    ("dropout_rate", (0.5,)),
    ("batch_size", (64, 256))
])

for params in hyper:
    gen.batch_size = params["batch_size"]
    model = create_model(params["embed_dim"], params["lstm_size"], params["dropout_rate"], maxlen)
    print("\n" + hyper.dump(params))
    print(model.summary())
    history = model.fit(gen, epochs=num_epochs, batch_size=params["batch_size"])
    plot_graphs(history)
    modelfile = hyper.pack(params) + ".h5"
    print(f"Saving model to {modelfile}")
    model.save(modelfile)
    if hyper.endOfCycle("batch_size"):
        save_graph(hyper, params, "batch_size")

